package com.example.weatherapp.models

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

/*
Copyright (c) 2019 Kotlin Data Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */


//data class DataModel (
//
//	@SerializedName("coord") var coord : com.example.weatherapp.models.Coord,
//	@SerializedName("weather") var weather : List<Weather>,
//	@SerializedName("base") var base : String,
//	@SerializedName("main") var main : Main,
//	@SerializedName("visibility") var visibility : Int,
//	@SerializedName("wind") var wind : Wind,
//	@SerializedName("clouds") var clouds : Clouds,
//	@SerializedName("dt") var dt : Int,
//	@SerializedName("sys") var sys : Sys,
//	@SerializedName("timezone") var timezone : Int,
//	@SerializedName("id") var id : Int,
//	@SerializedName("name") var name : String,
//	@SerializedName("cod") var cod : Int
//)

class DataModel {

	@SerializedName("coord")
	var coord: Coord? = null
	@SerializedName("sys")
	var sys: Sys? = null
	@SerializedName("weather")
	var weather = ArrayList<Weather>()
	@SerializedName("main")
	var main: Main? = null
	@SerializedName("wind")
	var wind: Wind? = null
	@SerializedName("rain")
	var rain: Rain? = null
	@SerializedName("clouds")
	var clouds: Clouds? = null
	@SerializedName("dt")
	var dt: Float = 0.toFloat()
	@SerializedName("id")
	var id: Int = 0
	@SerializedName("name")
	var name: String? = null
	@SerializedName("cod")
	var cod: Float = 0.toFloat()
}

class Weather {
	@SerializedName("id")
	var id: Int = 0
	@SerializedName("main")
	var main: String? = null
	@SerializedName("description")
	var description: String? = null
	@SerializedName("icon")
	var icon: String? = null
}

class Clouds {
	@SerializedName("all")
	var all: Float = 0.toFloat()
}

class Rain {
	@SerializedName("3h")
	var h3: Float = 0.toFloat()
}

class Wind {
	@SerializedName("speed")
	var speed: Float = 0.toFloat()
	@SerializedName("deg")
	var deg: Float = 0.toFloat()
}

class Main {
	@SerializedName("temp")
	var temp: Float = 0.toFloat()
	@SerializedName("humidity")
	var humidity: Float = 0.toFloat()
	@SerializedName("pressure")
	var pressure: Float = 0.toFloat()
	@SerializedName("temp_min")
	var temp_min: Float = 0.toFloat()
	@SerializedName("temp_max")
	var temp_max: Float = 0.toFloat()
}

class Sys {
	@SerializedName("country")
	var country: String? = null
	@SerializedName("sunrise")
	var sunrise: Long = 0
	@SerializedName("sunset")
	var sunset: Long = 0
}

class Coord {
	@SerializedName("lon")
	var lon: Float = 0.toFloat()
	@SerializedName("lat")
	var lat: Float = 0.toFloat()
}