package com.example.weatherapp.retrofit

import com.example.weatherapp.models.DataModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    //@GET("users")
    @GET("data/2.5/weather?")
    fun getInfo(@Query("id") cityID: String, @Query("units") unit: String, @Query("APPID") appID: String): Call<DataModel>
}