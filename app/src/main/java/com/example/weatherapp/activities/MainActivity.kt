package com.example.weatherapp.activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View.OnFocusChangeListener
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherapp.R
import com.example.weatherapp.adapters.RecyclerViewAdapter
import com.example.weatherapp.adapters.SearchAdapter
import com.example.weatherapp.classes.LoadJson
import com.example.weatherapp.interfaces.DetailClickListener
import com.example.weatherapp.interfaces.RecyclerViewItemClickListener
import com.example.weatherapp.models.DataModel
import com.example.weatherapp.retrofit.ApiClient
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


//https://stackoverflow.com/questions/39772090/android-studio-cant-find-my-json-file
//https://stackoverflow.com/questions/13814503/reading-a-json-file-in-android

class MainActivity : AppCompatActivity() {

    var context: Context = this
    lateinit var loadingEffect: ProgressDialog
    var dataArr = ArrayList<DataModel>()
    var dataArrForStore = ArrayList<DataModel>()
    var cityNameArr = ArrayList<String>()
    var filteredCityNames = ArrayList<String>()
    var cityIDArr = ArrayList<String>()
    var filteredCityID = ArrayList<String>()
    var emptyArr = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadingEffect = ProgressDialog(this)
        loadingEffect.setTitle("Loading City List..")
        loadingEffect.setCancelable(false)
        loadingEffect.show()

        //loading data from json file
        val ls = LoadJson()
        val arrData =  ls.loadJSONFromAsset(this)
        var dataArr2 = arrayOf(arrData)
        val data = JSONArray(dataArr2[0])

        var a = 0
        while (a < data.length()){
            val arrObject: JSONObject = data.getJSONObject(a)
            cityNameArr.add(arrObject.getString("name"))
            cityIDArr.add(arrObject.getString("id"))
            a++
        }

        loadingEffect.dismiss()

        rvResult.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        recyclerView.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        recyclerView.adapter = RecyclerViewAdapter(dataArr,this, object : DetailClickListener {
            override fun onWeatherSetClicked(cityIndex: Int) {
                goToActivityDetail(cityIndex)
            }
        })

//------------------------------------------------------------------------------------------------------------------------------------------------

        svSearchBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(query: String?): Boolean {
                if (query == null){
                    recyclerView.adapter = RecyclerViewAdapter(dataArrForStore,context, object : DetailClickListener {
                        override fun onWeatherSetClicked(cityIndex: Int) {
                            goToActivityDetail(cityIndex)
                        }
                    })
                }
                filterCityNames(query.toString())
                rvResult.layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
                rvResult.adapter = SearchAdapter(filteredCityNames,context, object : RecyclerViewItemClickListener {
                    override fun onClicked(cityIndex: Int) {
                        getData(filteredCityID[cityIndex],true)
                        rvResult.adapter = SearchAdapter(emptyArr,context, object : RecyclerViewItemClickListener {
                            override fun onClicked(cityIndex: Int) {
                                getData(filteredCityID[cityIndex],true)
                            }
                        })


                    }
                })
                return false
            }
        })

    }

    private fun getData(cityID:String,callFromFilter:Boolean) {
        val call: Call<DataModel> = ApiClient.getClient.getInfo(cityID,"metric","7daceca435034c3e6068aa8bea73cae9")
        call.enqueue(object : Callback<DataModel> {

            override fun onResponse(call: Call<DataModel>?, response: Response<DataModel>?) {
                //loadingEffect.dismiss()
                if (callFromFilter){
                    dataArr.clear()
                }

                dataArr.add(response?.body()!!)
                if (!callFromFilter){
                    dataArrForStore.add(response?.body()!!)
                }
                recyclerView.adapter?.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<DataModel>?, t: Throwable?) {
                loadingEffect.dismiss()
            }

        })
    }

    fun filterCityNames(inputText:String){
        filteredCityNames.clear()
        filteredCityID.clear()
        var txtInLower = inputText.toLowerCase(Locale.getDefault())
        if (txtInLower.isEmpty()){
//            filteredCityNames.addAll(cityNameArr)
//            filteredCityID.addAll(cityIDArr)
        }
        else{
            var i = 0
            while (i < cityNameArr.size){
                if(cityNameArr[i].toLowerCase(Locale.getDefault()).contains(txtInLower)){
                    filteredCityNames.add(cityNameArr[i])
                    filteredCityID.add(cityIDArr[i])
                }
                i++
            }
        }
    }

    private fun goToActivityDetail(cityId: Int) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("cityID", cityId)
        startActivity(intent)
    }

}
