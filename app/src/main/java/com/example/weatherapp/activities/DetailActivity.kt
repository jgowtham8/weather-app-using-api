package com.example.weatherapp.activities

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.weatherapp.R
import com.example.weatherapp.models.DataModel
import com.example.weatherapp.retrofit.ApiClient
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailActivity : AppCompatActivity() {

    lateinit var loadingEffect: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val cityID: Int? = intent.extras?.getInt("cityID")

        loadingEffect = ProgressDialog(this)
        loadingEffect.setTitle("Loading Data")
        loadingEffect.setCancelable(false)
        loadingEffect.show()

        getData(cityID.toString())

    }

    private fun getData(cityID:String) {
        val call: Call<DataModel> = ApiClient.getClient.getInfo(cityID,"metric","7daceca435034c3e6068aa8bea73cae9")
        call.enqueue(object : Callback<DataModel> {

            override fun onResponse(call: Call<DataModel>?, response: Response<DataModel>?) {
                loadingEffect.dismiss()

                tvTitle.text = response?.body()!!.name
                tvCityName.text = response?.body()!!.name
                tvCityID.text = response.body()!!.id.toString()
                tvStatus.text = response.body()!!.weather[0].main
                tvClouds.text = response.body()!!.clouds?.all.toString()
                tvWindSpeed.text = response.body()!!.wind?.speed.toString()
                tvTemp.text = response.body()!!.main?.temp.toString()
                tvHumidity.text = response.body()!!.main?.humidity.toString()
                tvPressure.text = response.body()!!.main?.pressure.toString()
                tvDes.text = response.body()!!.weather[0].description
                tvCountry.text = response.body()!!.sys?.country.toString()
                tvLat.text = response.body()!!.coord?.lat.toString()
                tvLon.text = response.body()!!.coord?.lon.toString()

                val iconName = response.body()!!.weather[0].icon.toString()
                val url = "http://openweathermap.org/img/w/$iconName.png"
                Picasso.get().load(url).into(icon)
            }

            override fun onFailure(call: Call<DataModel>?, t: Throwable?) {
                loadingEffect.dismiss()
                Toast.makeText(this@DetailActivity,"No response from Weather Service..!",Toast.LENGTH_LONG).show()
            }

        })
    }
}
