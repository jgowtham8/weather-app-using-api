package com.example.weatherapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.interfaces.DetailClickListener
import com.example.weatherapp.interfaces.RecyclerViewItemClickListener
import com.example.weatherapp.models.DataModel
import kotlinx.android.synthetic.main.widget_list_item.view.*

class RecyclerViewAdapter(private var dataList: List<DataModel>, private val context: Context,private val detailClickListener: DetailClickListener) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.widget_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel= dataList[position]

        holder.iD1.text = dataModel.id.toString()
        holder.iD2.text = dataModel.name
        holder.iD3.text = dataModel.main?.temp.toString()
        holder.iD4.text = dataModel.weather[0].description
        holder.iD5.text = dataModel.main?.humidity.toString()
        holder.rowFrame.setOnClickListener {
            detailClickListener.onWeatherSetClicked(dataModel.id)
        }

    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val iD1: TextView = view.tvCityID
    val iD2: TextView = view.tvCityName
    val iD3: TextView = view.tvTemp
    val iD4: TextView = view.tvDes
    val iD5: TextView = view.tvHumidity
    val rowFrame: View = view

}