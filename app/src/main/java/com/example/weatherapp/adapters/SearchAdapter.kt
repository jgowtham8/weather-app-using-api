package com.example.weatherapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.interfaces.RecyclerViewItemClickListener
import com.example.weatherapp.models.DataModel
import kotlinx.android.synthetic.main.widget_search_item.view.*

class SearchAdapter (private var dataList: List<String>, private val context: Context,
                     private val recyclerViewItemClickListener: RecyclerViewItemClickListener
) : RecyclerView.Adapter<ViewHolder2>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
        return ViewHolder2(LayoutInflater.from(context).inflate(R.layout.widget_search_item, parent, false))
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder2, position: Int) {
        holder.item.text = dataList[position]
        holder.rowFrame.setOnClickListener {
            recyclerViewItemClickListener.onClicked(position)
        }
    }
}

class ViewHolder2 (view: View) : RecyclerView.ViewHolder(view) {
    val item: TextView = view.tvSearchItem
    val rowFrame:View = view
}