package com.example.weatherapp.interfaces

interface DetailClickListener {
    fun onWeatherSetClicked(cityIndex: Int)
}