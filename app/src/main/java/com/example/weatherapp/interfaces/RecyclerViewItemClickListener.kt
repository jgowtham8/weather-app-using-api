package com.example.weatherapp.interfaces

interface RecyclerViewItemClickListener {
    fun onClicked(cityIndex: Int)

}